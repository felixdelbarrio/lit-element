import {LitElement, html} from 'lit-element'


class FichaPersona extends LitElement {

    static get properties() {
        
        return {
            name : {type: String},
            yearsInCompany:  {type: Number},
            personInfo: {type: String},
            photo:  {type: Object}
        }
    }

    constructor() {
        super()

        this.name = 'Prueba nombre'
        this.yearsInCompany = 1
        this.updatePersonInfo()
    }

    render(){
        return html`
        <div>    
            <label>Nombre completo</label>
            <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
            <br />
            <label>Años en la empresa</label>
            <input type="number" name="yearsInCompany" value="${this.yearsInCompany}" @change="${this.updateyearsInCompany}"}></input>
            <br />
            <input type="text" value="${this.personInfo}" disabled></input>
            <br />
        <div>    
        `
    }

    updated(changedProperties){
        console.log('updated')
        //changedProperties.forEach((oldValue, propName) => {
        //    console.log(`Propiedad  ${propName} cambia valor, anterior era ${oldValue} y el nuevo es ${this.name}`)
        //})

        if (changedProperties.has('yearsInCompany')){
            console.log(`Propiedad  yearsInCompany cambia valor, anterior era ${changedProperties.get('yearsInCompany')} y el nuevo es ${this.yearsInCompany}`)
            this.updatePersonInfo()
        }
    }

    updateName(e){
        console.log('updateName')
        this.name = e.target.value

    }

    updateyearsInCompany(e){
        console.log('updateYearsInCompany')
        this.yearsInCompany = e.target.value
        

    }
    updatePersonInfo(){
        console.log('datePersonInfo')
        console.log(`yearsInCompany vale ${this.yearsInCompany}`)

        if(this.yearsInCompany >=7){
            this.personInfo = 'lead'
        }else if(this.yearsInCompany >=5){
            this.personInfo = 'senior'
        }else if(this.yearsInCompany >=3){
            this.personInfo = 'team'
        }else {
            this.personInfo = 'junior'
        }
    }
}

customElements.define('ficha-persona', FichaPersona)