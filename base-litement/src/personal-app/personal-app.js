import {LitElement, html} from 'lit-element'

import '../personal-header/personal-header.js'
import '../personal-main/personal-main.js'
import '../personal-footer/personal-footer.js'
import '../personal-sidebar/personal-sidebar.js'
import '../personal-stats/personal-stats.js'


class PersonalApp extends LitElement {


    static get properties(){
        return {
            people: {type : Array}
        }
    }

    constructor() {
        super()

        this.people = []
    }
    
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">         
        <personal-header>
        </personal-header>
            <div class="row">
                <personal-sidebar 
                    @new-person="${this.newPerson}" 
                    class="col-2"
                >
                </personal-sidebar>
                <personal-main 
                    @updated-people ="${this.updatedPeople}"
                    class="col-10"
                >
                </personal-main>
            </div>
        <personal-footer>
        </personal-footer>
        <personal-stats
            @updated-people-stats='${this.peopleStatsUpdated}'>
        </personal-stats>     
        `
    }

    updated(changedProperties) {
        console.log('updated en persona-app')

        if (changedProperties.has('people')){
            console.log('Ha cambiado el valor de la propiedad people en persona-app')
            this.shadowRoot.querySelector('personal-stats').people = this.people

        }
    }

    peopleStatsUpdated(e){
        console.log('peopleStatsUpdated')
        console.log(e.detail)

        this.shadowRoot.querySelector('personal-sidebar').peopleStats = e.detail.peopleStats
    }
    updatedPeople(e){
        console.log('updatedPeople')
        this.people = e.detail.people
    }
    newPerson(e) {
        console.log('newPerson en persona-app')
        console.log(e)

        this.shadowRoot.querySelector('personal-main').showPersonForm = true
    }

    
}

customElements.define('personal-app', PersonalApp)