import {LitElement, html} from 'lit-element'


class PersonalForm extends LitElement {


    static get properties(){
        return {
            person: {type : Object},
            editingPerson: {type : Boolean}
        }
    }

    constructor() {
        super()

        this.resetFormData()
        this.editingPerson = false
    }
    
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">             
        <div>
            <form>
                <div class="form-group">
                    <label>Nombre commpleto</label>
                    <input type="text" 
                        @input="${this.updateName}" 
                        .value="${this.person.name}" 
                        ?disabled=${this.editingPerson}
                        class="form-control" 
                        placeholder="Nombre completo" 
                        id="nombre-completo"/ >
                </div>
                    <label>Perfil</label>
                    <textarea 
                        @input="${this.updateProfile}" 
                        .value="${this.person.profile}" 
                        class="form-control" 
                        placeholder="Perfil" 
                        rows="5" 
                        id="perfil">
                    </textarea>
                <div class="form-group"> 
                    <label>Años en la empresa</label>
                    <input 
                        @input="${this.updateYearsInCompany}" 
                        .value="${this.person.yearsInCompany}" 
                        type="text" 
                        class="form-control" 
                        placeholder="Años en la empresa" 
                        id="antiguedad"/ >
                </div>
                <button  
                    @click="${this.goBack}" 
                    class="btn-primary">
                        <strong>Atrás</strong>
                </button>
                <button  
                    @click="${this.storePerson}" 
                    class="btn-success">
                        <strong>Guardar</strong>
                    </button>
            </form>
        </div>
        `
    }

    updated(changedProperties){
        
        console.log('changedProperties')

    }
    
    updateName(e){
        console.log('updatename')
        console.log('Actualizando la propiedad name de la persona con el valor ' + e.target.value)
        this.person.name = e.target.value
    }
    updateProfile(e){
        console.log('updateProfile')
        console.log('Actualizando la propiedad profile de la persona con el valor ' + e.target.value)
        this.person.profile = e.target.value
    }   

    updateYearsInCompany(e){
        console.log('updateYearsInCompany')
        console.log('Actualizando la propiedad años en la compañia de la persona con el valor ' + e.target.value)
        this.person.yearsInCompany = e.target.value
    }   

    goBack(e){
        
        console.log('go-back')
        console.log(e)
        e.preventDefault()

        this.resetFormData()
        this.dispatchEvent(new CustomEvent('persona-form-close'))
        

    }

    storePerson(e){
        console.log('store-person')
        console.log(e)
        e.preventDefault()

        this.person.photo = {
            'src' : '../img/persona2.jpg',
            'alt' : 'Persona'
        }

        console.log('la propiedad name en person vale ' + this.person.name)
        console.log('la propiedad profile en person vale ' + this.person.profile)
        console.log('la propiedad yearsInCompany en person vale ' + this.person.yearsInCompany)

        this.dispatchEvent(new CustomEvent('persona-form-store', {
            detail: {
                person : {
                    name: this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo:this.person.photo
                },
                editingPerson: this.editingPerson
            }
        }))

        this.resetFormData()
        
    }

    resetFormData() {
        
        console.log('resetFormData')

        this.editionPerson = false
        this.person = {}

        this.person.name = ''
        this.person.profile = ''
        this.person.yearsInCompany = ''
    }

}

customElements.define('personal-form', PersonalForm)