import {LitElement, html} from 'lit-element'
import '../personal-ficha-listado/personal-ficha-listado.js'
import '../personal-form/personal-form.js'

class PersonalMain extends LitElement {

    static get properties(){
        return {
            people : {type: Array},
            showPersonForm : {type: Boolean}
        }
    }

    constructor() {
        super()

        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany: 10,
                profile: "Lorem ipsum dolor dit amet.",
                photo: {
                    src: "/img/persona.jpeg",
                    alt: "Ellen Ripley"
                }
            },
            {
                name: "Bruce Banner",
                yearsInCompany: 2,
                profile: "En un lugar de la mancha.",
                photo: {
                    src: "/img/persona.jpeg",
                    alt: "Bruce Banner"
                }
            },
            {
                name: "Eowyn",
                yearsInCompany: 5,
                profile: "De cuyo nombre no quiero acordarme.",
                photo: {
                    src: "./img/persona.jpeg",
                    alt: "Eowyn"
                }
            },
            {
                name: "Felix",
                yearsInCompany: 15,
                profile: "Vivia un Caballero...",
                photo: {
                    src: "./img/persona.jpeg",
                    alt: "Felix del Barrio"
                }
            },
            {
                name: "Maria",
                yearsInCompany: 14,
                profile: "Lorem ipsum dolor dit amet.",
                photo: {
                    src: "./img/persona.jpeg",
                    alt: "Maria Aristizabal"
                }
            }
        ]

        this.showPersonForm = false
    }
    
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous"> 
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html`<personal-ficha-listado 
                            fname ="${person.name}"
                            yearsInCompany="${person.yearsInCompany}"
                            profile="${person.profile}"
                            .photo="${person.photo}"
                            @delete-person="${this.deletePerson}"
                            @info-person="${this.infoPerson}"
                        >
                        </personal-ficha-listado>`
                    )}
                </div>    
            </div>
            <div class="row">
                <personal-form  
                    @persona-form-close="${this.personFormClose}" 
                    @persona-form-store="${this.personFormStore}"                
                    class="d-none border rounded border-primary" 
                    id="personaForm"
                >
                </personal-form> 
            <div>     
        `
    }
    
    updated(changedProperties){
        
        console.log('updated')
        console.log(changedProperties)

        if (changedProperties.has("showPersonForm")){
            console.log('updated showPersonForm')
            this.showPersonForm === true ? this.showPersonFormData() : this.showPersonList()
        }
        
        if (changedProperties.has("people")){
            console.log('updated people')
            this.dispatchEvent(new CustomEvent('updated-people', {

                'detail': {
                    people: this.people
                }
            }))
        }
    }
    
    personFormClose(){
        console.log('personFormClose')
        console.log('Se ha cerrado el formulario de la persona')
        this.showPersonForm = false
    }
    
    showPersonFormData() {
        console.log('showPersonFormData')
        console.log('mostrar formulario de persona')

        this.shadowRoot.getElementById('personaForm').classList.remove('d-none')
        this.shadowRoot.getElementById('peopleList').classList.add('d-none')

    }

    showPersonList() {
        console.log('showPersonList')
        console.log('mostrar listado de personas')

        this.shadowRoot.getElementById('personaForm').classList.add('d-none')
        this.shadowRoot.getElementById('peopleList').classList.remove('d-none')

    }


    personFormStore(e){
        console.log('personFormStore')
        console.log('Se va a almacenar la persona')    
        console.e

        console.log('Name ' + e.detail.person.name)
        console.log('profile ' + e.detail.person.profile)
        console.log('yearsInCompany ' + e.detail.person.yearsInCompany)
        console.log('la propiedad editing person vale ' + e.detail.editingPerson)

        if (e.detail.editingPerson === true) {
            console.log('Se va a actualizar la persona de nombre ' +e.detail.person.name) 


            // Refactorizamos código para que se produzca una reasignacion y Cells capture
            //la misma de manera que Lit detecte que hay un cambio en el array y por ende
            // Se llame a updated
            // let indexOfPerson = this.people.findIndex(
            //     person=> person.name === e.detail.person.name
            // )
    
            // if (indexOfPerson >= 0) {
            //     console.log("persona  encontrada")
            //     this.people[indexOfPerson] = e.detail.person
                 
            // }else{
            //     console.log("persona no encontrada")
            // }

            this.people = this.people.map(
                person => person.name === e.detail.person.name
                            ? person = e.detail.person : person
            )
            

        } else {
            console.log('Se va a crear una persona nueva')
             // Refactorizamos código para que se produzca una reasignacion y Cells capture
            //la misma de manera que Lit detecte que hay un cambio en el array y por ende
            // Se llame a updated
            //this.people.push(e.detail.person)
            //Esta forma de detallar la instancia es crear una nueva instancia del que existía
            //con el nuevo elemento al final
            this.people = [...this.people , e.detail.person]
        }

        console.log('Persona almacenada')

        this.showPersonForm = false
    }

    deletePerson(e) {
        console.log('deletePerson en persona-main')
        console.log('Se va a borrar la persona de nombre' + e.detail.name)

        //como el componente está dentro del shadow root podemos acceder a sus propiedades
        this.people = this.people.filter(
            person => person.name != e.detail.name
        )

    }

    infoPerson(e) {
        console.log('infoPerson en persona-main')
        console.log('Se solicita información de la persona de nombre' + e.detail.name)

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        )

        console.log(chosenPerson)

        this.showPersonForm = true

        //Se asiga a otro objeto para que no use la posición de memoria y no se actualice cuando no debe
        let personToShow = {}

        personToShow.name = chosenPerson[0].name
        personToShow.profile = chosenPerson[0].profile
        personToShow.yearsInCompany = chosenPerson[0].yearsInCompany


        this.shadowRoot.getElementById('personaForm').person = personToShow
        this.shadowRoot.getElementById('personaForm').editingPerson = true


    }

}

customElements.define('personal-main', PersonalMain)